# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.

class TowersOfHanoi
  attr_reader :tower

  def initialize
    @tower = [[3,2,1],[],[]] #default creation
  end

  def first
    @tower[0]
  end


  def play
    until won?
      self.render
      puts "next move, which tower do you want to move"
      start_tower = gets
      puts "and where do you want to move it to"
      end_tower = gets
      move(start_tower.to_i,end_tower.to_i)
      puts
    end
    puts "you've won!"
  end

  def render
    (0..2).each do |tower_num|
      if @tower[tower_num].length == 3
        puts "3 2 1"
      elsif @tower[tower_num].length == 2
        puts @tower[tower_num][0].to_s + ' ' + @tower[tower_num][1].to_s
      elsif @tower[tower_num].length == 1
        puts @tower[tower_num][0].to_s
      else
        puts "     "
      end
    end
  end

  #  puts @tower[]

  def move(tow1,tow2)
    if valid_move?(tow1,tow2)
      @tower[tow2] << @tower[tow1].last
      @tower[tow1].pop
    end
  end

  def valid_move?(tow1,tow2)
    if @tower[tow2] == []
      true
    elsif @tower[tow1] == []
      false
    else
      @tower[tow1][-1] < @tower[tow2][-1]
    end
  end

  def won?
    if @tower[1] == [3,2,1] || @tower[2] == [3,2,1]
      true
    else
      false
    end
  end

  def towers
    @tower
  end


end
tower = TowersOfHanoi.new
tower.play
